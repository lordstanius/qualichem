package myApp.qualychem;

public class Stack<T> {
		private class Elem {
		T value;
		Elem last;

		Elem(T value, Elem last) {
			this.value = value;
			this.last = last;
		}
		
	}
	
	private Elem top;
	private int size = 0;
	
	public boolean isEmpty() {
		return (top == null);
	}
	
	public void push(T value) {
		top = new Elem(value, top);
		++size;
		//System.out.printf("Element is pushed on the stack. Total %s elements",++i);
	}
	
	public T pop() {
		T retVal = top.value;
		top = top.last;
		--size;
		
		//System.out.printf("Element is poped off the stack. Total %s elements",--i);
		return retVal;
	}
	
	public T getTop() {
		return top.value;
	}
	
	public int size() {
		return size;
	}
}
