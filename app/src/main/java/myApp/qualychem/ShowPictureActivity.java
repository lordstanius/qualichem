package myApp.qualychem;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

public class ShowPictureActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_picture_layout);
		
		int imageId = getIntent().getExtras().getInt("myApp.qualychem.ImageName");
		String activityName = getIntent().getExtras().getString("myApp.qualychem.ActivityName");
		
		((ImageView) findViewById(R.id.image)).setImageResource(imageId);
		
		setTitle(activityName);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_picture, menu);
		return true;
	}

}
