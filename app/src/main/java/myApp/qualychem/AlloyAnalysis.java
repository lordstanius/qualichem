package myApp.qualychem;

import android.view.View;
import android.view.View.OnClickListener;


public class AlloyAnalysis {
	AnionAnalysis  anions;
	CationAnalysis cations;
	Controller c;
	
	boolean testForPO4 = false;

	AlloyAnalysis(Controller controller) {
		anions = new AnionAnalysis(controller);
		cations = new CationAnalysis(controller, this);
		c = controller;
	}

	void start() {
		c.setSteps(null, step1);
		c.setMessage(R.string.msg_wellcome_alloy);
		c.hideOptions();
		c.hideNote();
	}

	OnClickListener step1 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title1);
			c.setMessage(R.string.al_step1);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step2);
		}
	};

	OnClickListener step2 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title1);
			c.setMessage(R.string.al_step2, R.drawable.talog);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step2option);
		}
	};

	OnClickListener step2option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {				
				step8.onClick(v);
				testForPO4 = true;
			} else {
				step3.onClick(v);
			}
		}
	};

	OnClickListener step3 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title3);
			c.setMessage(R.string.al_step3);
			c.showOptions();
			c.showNote(R.string.al_note3);
			c.setSteps(this, step3option);
		}
	};

	OnClickListener step3option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step5.onClick(v);
			} else {
				step4.onClick(v);
			}	
		}
	};

	OnClickListener step4 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title4);
			c.setMessage(R.string.al_step4);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step5);						
		}
	};

	OnClickListener step5 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title5);
			c.setMessage(R.string.al_step5);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, cations.ca2.step18);						
		}
	};

//	OnClickListener step5option = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			if (c.isYes()) {
//				cations.ca2.step18.onClick(v);
//			} else {
//				step6b.onClick(v);
//			}	
//		}
//	};	

	OnClickListener step6 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (testForPO4)
				c.setTitle(R.string.al_title6a);
			else
				c.setTitle(R.string.al_title6);
			c.setMessage(R.string.al_step6);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step7);						
		}
	};

//	OnClickListener step6b = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.al_title6b);
//			c.setMessage(R.string.al_step6);
//			c.hideOptions();
//			c.hideNote();
//			c.setSteps(this, step7b);						
//		}
//	};
	
	OnClickListener step7 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			anions.remove(R.string.phosphate);
			if (testForPO4)
				c.setTitle(R.string.al_title6a);
			else
				c.setTitle(R.string.al_title6);
			c.setMessage(R.string.al_step7);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step7option);					
		}
	};

//	OnClickListener step7b = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			anions.remove(R.string.phosphate);
//			c.setTitle(R.string.al_title6b);
//			c.setMessage(R.string.al_step7);
//			c.showOptions();
//			c.hideNote();
//			c.setSteps(this, step7option);					
//		}
//	};
	
	OnClickListener step7option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				anions.add(R.string.phosphate);
			}
			
			if (testForPO4)
				step9.onClick(v);
			else
				step8.onClick(v);
		}
	};

	OnClickListener step8 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title8);
			c.setMessage(R.string.al_step8);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, cations.ca2.step2);					

		}
	};
	
	OnClickListener step9 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			cations.remove(R.string.Fe);
			c.setTitle(R.string.al_title9);
			c.setMessage(R.string.al_step9);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step9option);
		}	
	};
	
	OnClickListener step9option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				cations.add(R.string.Fe);
				step10.onClick(v);				
			} else {
				step10a.onClick(v);
			}
		}	
	};
	
	OnClickListener step10 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title10);
			c.setMessage(R.string.al_step10);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step11);
		}	
	};

	OnClickListener step10a = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title10);
			c.setMessage(R.string.al_step10a);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step11);
		}	
	};
	
	OnClickListener step11 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title10);
			c.setMessage(R.string.al_step11);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step12);
		}	
	};

	OnClickListener step12 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.al_title10);
			c.setMessage(R.string.al_step12);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, cations.ca3.step3);
		}	
	};	
	
	final OnClickListener outro = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setSteps(this, endAlloy);
			c.setMessage(R.string.msg_outro_alloy);
			c.hideTitle();		
			c.hideNote();
			c.hideOptions();
		}
	};
	
	final OnClickListener endAlloy = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.exit();
		}
	};	
}









