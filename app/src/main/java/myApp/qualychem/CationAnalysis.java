package myApp.qualychem;

import android.view.View;
import android.view.View.OnClickListener;

public class CationAnalysis extends Analysis {
	Group1 ca1 = new Group1();
	Group2 ca2 = new Group2();
	Group3 ca3 = new Group3();
	Group4 ca4 = new Group4();
	Group5 ca5 = new Group5();

	AlloyAnalysis        alloy = null;
	OneComponentAnalysis oneComp = null;
	
	// ---------------------------------------
	// Constructors
	// ---------------------------------------
	public CationAnalysis(Controller controller) {
		super(controller);
	}
	
	public CationAnalysis(Controller controller, AlloyAnalysis alloy) {
		super(controller);
		this.alloy = alloy;		
	}
	
	public CationAnalysis(Controller controller, OneComponentAnalysis oneComp) {
		super(controller);
		this.oneComp = oneComp;		
	}
	
	public void start() {
		c.setSteps(null, ca1.step1);
		c.setMessage(R.string.msg_wellcome_cations);
		c.hideOptions();
		c.hideNote();
	}

	public void refreshContent() {
		if (!isEmpty()) {
			c.writeCations(this);
			c.showCations();
		} else {
			c.hideCations();
		}
	}		
	
	class Group1 {
		final OnClickListener step1 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title1);
				c.setMessage(R.string.ca1_step1);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step2);
			}
		};

		final OnClickListener step2 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.NH4);
				c.setTitle(R.string.ca1_title2);
				c.setMessage(R.string.ca1_step2, R.drawable.talog);
				c.showExtendedOptions(R.string.ca1_step2_option);
				c.hideNote();
				c.setSteps(this, step2option);
			}
		};

		final OnClickListener step2option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.NH4);
					step5.onClick(v);
					break;
				case 1:
					step5.onClick(v);
					break;
				case 2:
					step3.onClick(v);
					break;
				}
			}
		};

		
		final OnClickListener step3 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title3);
				c.setMessage(R.string.ca1_step3);
				remove(R.string.NH4);
				c.showExtendedOptions(R.string.str_try_another);
				c.hideNote();
				c.setSteps(this, step3option);
			}
		};

		final OnClickListener step3option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.NH4); // one option is proof, the others are not!
					step5.onClick(v); 					
					break;
				case 1:
					remove(R.string.NH4);
					step5.onClick(v);					
					break;
				case 2:
					step4.onClick(v);
				}
			}
		};	
		
		final OnClickListener step4 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.NH4);
				c.setTitle(R.string.ca1_title4);
				c.setMessage(R.string.ca1_step4);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step4option);
			}
		};

		final OnClickListener step4option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.NH4);
				} else {
					remove(R.string.NH4);
				}
				step5.onClick(v);
			}
		};		
		
		
		final OnClickListener step5 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (oneComp != null) {
					if (isProven(R.string.NH4))
						oneComp.stepAA.onClick(v);
					else
						oneComp.step1.onClick(v);
				} else {
					c.setTitle(R.string.ca1_title5);
					c.setMessage(R.string.ca1_step5);
					c.showOptions();
					c.hideNote();
					c.setSteps(this, step5option);
				}
			}
		};

		final OnClickListener step5option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step10.onClick(v);
				} else {
					step6.onClick(v);
				}
			}
		};

		final OnClickListener step6 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title5);
				c.setMessage(R.string.ca1_step6);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step6option);
			}
		};

		final OnClickListener step6option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step10.onClick(v);
				} else {
					step7.onClick(v);
				}
			}
		};
		
		final OnClickListener step7 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title5);
				c.setMessage(R.string.ca1_step7);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step10);
			}
		};

		final OnClickListener step10 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title10);
				c.setMessage(R.string.ca1_step10);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step10option);
			}
		};

		final OnClickListener step10option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step11.onClick(v);
				} else {
					ca2.step1.onClick(v);
				}
			}
		};

		final OnClickListener step11 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title11);
				c.setMessage(R.string.ca1_step11);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step12);
			}
		};

		final OnClickListener step12 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isProven(R.string.Pb)) {
					c.setTitle(R.string.ca1_title12a);
					c.setMessage(R.string.ca1_step12a);
				} else {
					c.setTitle(R.string.ca1_title12);
					c.setMessage(R.string.ca1_step12);
				}
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step12option);
			}
		};

		final OnClickListener step12option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isProven(R.string.Pb)) {
					if (c.isYes()) {
						ca2.step1.onClick(v);
					} else {
						step14.onClick(v);
					}
				} else {
					if (c.isYes()) {
						step13a.onClick(v);
					} else {
						step13b.onClick(v);
					}					
				}
			}
		};

		final OnClickListener step13a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Pb);
				c.setTitle(R.string.ca1_title13);
				c.setMessage(R.string.ca1_step13);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step13aOption);
			}
		};

		final OnClickListener step13aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Pb);
				}
				ca2.step1.onClick(v);
			}
		};

		final OnClickListener step13b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Pb);
				c.setTitle(R.string.ca1_title13);
				c.setMessage(R.string.ca1_step13);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step13bOption);
			}
		};

		final OnClickListener step13bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Pb);
					step12.onClick(v);				
				} else {
					step14.onClick(v);
				}
			}
		};

		final OnClickListener step14 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title14);
				c.setMessage(R.string.ca1_step14);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step14option);
			}
		};

		final OnClickListener step14option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step15a.onClick(v);
				} else {
					step15b.onClick(v);
				}
			}
		};

		final OnClickListener step15a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Ag);
				c.setTitle(R.string.ca1_title15);
				c.setMessage(R.string.ca1_step15);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step15aOption);
			}
		};

		final OnClickListener step15aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ag);
				}
				ca2.step1.onClick(v);
			}
		};

		final OnClickListener step15b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Ag);
				c.setTitle(R.string.ca1_title15);
				c.setMessage(R.string.ca1_step15);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step15bOption);
			}
		};

		final OnClickListener step15bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ag);
				}
				step16.onClick(v);
			}
		};

		final OnClickListener step16 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Hg1);
				c.setTitle(R.string.ca1_title16);
				c.setMessage(R.string.ca1_step16);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step16option);
			}
		};

		final OnClickListener step16option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Hg1);
				}
				step17.onClick(v);
			}
		};

		final OnClickListener step17 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca1_title17);
				c.setMessage(R.string.ca1_step17);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, ca2.step1);
			}
		};
	} // end of Group 1


	class Group2 {	
		final OnClickListener step1 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title1);
				c.setMessage(R.string.ca2_step1);
				c.hideOptions();
				c.showNote(R.string.ca2_note1);
				c.setSteps(this, step2);
			}
		};

		final OnClickListener step2 = new OnClickListener() {		
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title2);
				c.setMessage(R.string.ca2_step2);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step3);
			}
		};

		final OnClickListener step3 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title2);
				c.setMessage(R.string.ca2_step3);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step3option);
			}
		};

		final OnClickListener step3option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step5.onClick(v);
				} else {
					step4.onClick(v);
				}
			}
		};

		final OnClickListener step4 = new OnClickListener() {	
			@Override
			public void onClick(View v) {
				if (alloy == null)
					c.setTitle(R.string.ca2_title4);
				else
					c.setTitle(R.string.ca2_title4a);
				c.setMessage(R.string.ca2_step4);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step4option);				
			}
		};

		final OnClickListener step4option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step6.onClick(v);
				} else {
					if (alloy == null)
						ca3.step1.onClick(v);
					else
						if (alloy.anions.isProven(R.string.phosphate))
							alloy.step9.onClick(v);
						else
							ca3.step1.onClick(v);							
				}
			}
		};

		final OnClickListener step5 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title4);
				c.setMessage(R.string.ca2_step5);		
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step4option);				
			}
		};

		final OnClickListener step6 = new OnClickListener() {
			@Override
			public void onClick(View v) {				
				c.setTitle(R.string.ca2_title6);
				if (alloy == null) {
					c.setMessage(R.string.ca2_step6);
					c.showOptions();
					c.setSteps(this, step6option);
				} else {
					c.setMessage(R.string.ca2_step6a);
					c.hideOptions();
					c.setSteps(this, step7);
				}
				
				c.showNote(R.string.ca2_note6);
				
			}
		};

		final OnClickListener step6option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step17.onClick(v);
				} else {
					step7.onClick(v);
				}
			}
		};

		final OnClickListener step7 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (alloy == null) {	
					c.setTitle(R.string.ca2_title7);
					c.setMessage(R.string.ca2_step7);
				} else {
					c.setTitle(R.string.ca2_title7a);
					c.setMessage(R.string.ca2_step7a);					
				}
					
				c.showOptions();
				c.showNote(R.string.ca2_note7);
				c.setSteps(this, step7option);				
			}			
		};

		final OnClickListener step7option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step8.onClick(v);
				} else {
					step10.onClick(v);
				}
			}
		};

		final OnClickListener step8 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title8);
				c.setMessage(R.string.ca2_step8);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step9);								
			}
		};

		final OnClickListener step9 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Hg2);
				c.setTitle(R.string.ca2_title9);
				c.setMessage(R.string.ca2_step9);
				c.showOptions();
				c.showNote(R.string.ca2_note9);
				c.setSteps(this, step9option);
			}
		};

		final OnClickListener step9option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Hg2);
				}
				step10.onClick(v);
			}
		};

		final OnClickListener step10 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title10);
				c.setMessage(R.string.ca2_step10);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step10option);				
			}
		};

		final OnClickListener step10option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step11.onClick(v);
				} else {
					step14.onClick(v);
				}
			}
		};

		final OnClickListener step11 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title11);
				c.setMessage(R.string.ca2_step11);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step11option);				
			}
		};

		final OnClickListener step11option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step12.onClick(v);
				} else {
					step13.onClick(v);
				}
			}
		};

		final OnClickListener step12 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Bi);
				c.setTitle(R.string.ca2_title12);
				c.setMessage(R.string.ca2_step12);
				c.showOptions();
				c.showNote(R.string.ca2_note12);
				c.setSteps(this, step12option);												
			}
		};

		final OnClickListener step12option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Bi);
				}
				step13.onClick(v);				
			}
		};

		final OnClickListener step13 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Pb);
				c.setTitle(R.string.ca2_title13);
				c.setMessage(R.string.ca2_step13);
				c.showOptions();
				c.showNote(R.string.ca2_note13);
				c.setSteps(this, step13option);					
			}
		};

		final OnClickListener step13option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Pb);
				}
				step14.onClick(v);
			}
		};

		final OnClickListener step14 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Cu);
				c.setTitle(R.string.ca2_title14);
				c.setMessage(R.string.ca2_step14);
				c.showOptions();
				c.hideNote();
				c.setSteps(this, step14option);							
			}
		};

		final OnClickListener step14option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Cu);
					step15.onClick(v);
				} else {
					step16.onClick(v);
				}
			}
		};

		final OnClickListener step15 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title14);
				c.setMessage(R.string.ca2_step15);
				c.hideOptions();
				c.hideNote();
				c.setSteps(this, step16);											
			}
		};

		final OnClickListener step16 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Cd);
				c.setTitle(R.string.ca2_title14);
				c.setMessage(R.string.ca2_step16);
				c.showNote(R.string.ca2_note16);
				c.showOptions();

				c.setSteps(this, step16option);											
			}
		};

		final OnClickListener step16option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Cd);
				}
				
				if (alloy == null)
					step17.onClick(v);
				else {
					if (alloy.anions.isProven(R.string.phosphate))
						alloy.step9.onClick(v);						
					else
						step17.onClick(v);
				}
			}
		};

		final OnClickListener step17 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title17);
				c.setMessage(R.string.ca2_step17);
				c.showNote(R.string.ca2_note17);
				c.showOptions();
				c.setSteps(this, step17option);
			}
		};
		
		final OnClickListener step17option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step18.onClick(v);
				} else {
					ca3.step1.onClick(v);
				}
			}
		};

		final OnClickListener step18 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (alloy == null) {
					c.setTitle(R.string.ca2_title18);
					c.setMessage(R.string.ca2_step18);
					c.setSteps(this, step19);
				} else {
					c.setTitle(R.string.ca2_title18b);
					c.setMessage(R.string.ca2_step18b);
					c.setSteps(this, step21);
				}
				c.hideNote();
				c.hideOptions();
			}
		};

		final OnClickListener step19 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca2_title19);
				c.setMessage(R.string.ca2_step19);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step20a);			
			}
		};

		final OnClickListener step20a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.As);
				c.setTitle(R.string.ca2_title20a);
				c.setMessage(R.string.ca2_step20a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step20aOption);						
			}
		};

		final OnClickListener step20aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.As);
					step21.onClick(v);
					break;
				case 1:
					step21.onClick(v);
					break;
				case 2:
					step20b.onClick(v);
				}
			}
		};

		final OnClickListener step20b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.As);
				c.setTitle(R.string.ca2_title20b);
				c.setMessage(R.string.ca2_step20b);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step20bOption);															
			}
		};

		final OnClickListener step20bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.As);
				} 
				step21.onClick(v);			
			}
		};

		final OnClickListener step21 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (alloy == null)
					c.setTitle(R.string.ca2_title21);
				else
					c.setTitle(R.string.ca2_title21a);
				c.setMessage(R.string.ca2_step21);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step22);																		
			}
		};

		final OnClickListener step22 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Sb);
				c.setTitle(R.string.ca2_title22);
				c.setMessage(R.string.ca2_step22);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step22option);																		
			}
		};	

		final OnClickListener step22option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Sb);
				}
				step23.onClick(v);
			}
		};

		final OnClickListener step23 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Sn);
				c.setTitle(R.string.ca2_title23);
				c.setMessage(R.string.ca2_step23);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step23option);																								
			}
		};

		final OnClickListener step23option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Sn);
				}
				if (alloy == null)
					ca3.step1.onClick(v);
				else
					alloy.step6.onClick(v);
			}
		};
	} // end of group 2

	class Group3 {
		
		final OnClickListener step1 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title1);
				c.setMessage(R.string.ca3_step1);
				c.showNote(R.string.ca3_note1);
				c.hideOptions();
				c.setSteps(this, step2);
			}
		};
		
		final OnClickListener step2 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title2);
				c.setMessage(R.string.ca3_step2);
				c.showNote(R.string.ca3_note2);
				c.showOptions();
				c.setSteps(this, step2option);				
			}
		};
		
		final OnClickListener step2option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step3.onClick(v);
				} else {
					step10.onClick(v);
				}				
			}
		};
		
		final OnClickListener step3 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title3);
				if (alloy == null) {
					c.setMessage(R.string.ca3_step3);
					c.showOptions();
					c.setSteps(this, step3option);
				} else {
					c.setMessage(R.string.ca3_step3a);
					c.hideOptions();
					c.setSteps(this, step7);
				}
				c.hideNote();				
			}
		};
		
		final OnClickListener step3option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step4.onClick(v);
				} else {
					step7.onClick(v);
				}				
			}
		};
		
		final OnClickListener step4 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title4);
				c.setMessage(R.string.ca3_step4);
				c.hideNote();
				c.hideOptions();
				if (alloy == null)
					c.setSteps(this, step5);
				else
					c.setSteps(this, step6);
				
			}			
		};
		
		final OnClickListener step5 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title5);
				c.setMessage(R.string.ca3_step5);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step5a);									
			}				
		};
		
		final OnClickListener step5a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Fe);
				c.setTitle(R.string.ca3_title5a);
				c.setMessage(R.string.ca3_step5a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step5aOption);									
			}			
		};
		
		final OnClickListener step5aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Fe);
					step6.onClick(v);
					break;
				case 1:
					step6.onClick(v);
					break;
				case 2:
					step5b.onClick(v);
				}
			}			
		};
		
		final OnClickListener step5b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Fe);
				c.setTitle(R.string.ca3_title5b);
				c.setMessage(R.string.ca3_step5b);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step5bOption);									
			}			
		};
		
		final OnClickListener step5bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Fe);
				}
				step6.onClick(v);					
			}
		};
		
		final OnClickListener step6 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title6);
				c.setMessage(R.string.ca3_step6);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step6a);	
			}
		};
		
		final OnClickListener step6a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Mn);
				c.setTitle(R.string.ca3_title6a);
				c.setMessage(R.string.ca3_step6a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step6aOption);
			}
		};
		
		final OnClickListener step6aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Mn);
					step7.onClick(v);
					break;
				case 1:
					step7.onClick(v);
					break;
				case 2:
					step6b.onClick(v);
				}				
			}
		};
		
		final OnClickListener step6b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Mn);
				c.setTitle(R.string.ca3_title6b);
				c.setMessage(R.string.ca3_step6b);
				c.showNote(R.string.ca3_note6b);
				c.showOptions();
				c.setSteps(this, step6bOption);				
			}
		};
		
		final OnClickListener step6bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Mn);					
				} 				
				step7.onClick(v);
			}
		};
				
		final OnClickListener step7 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Cr);
				c.setTitle(R.string.ca3_title7);
				c.setMessage(R.string.ca3_step7);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step7option);					
			}
		};
		
		final OnClickListener step7option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Cr);
					step8.onClick(v);
				} else {
					step9a.onClick(v);
				}
			}						
		};
		
		final OnClickListener step8 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title8);
				c.setMessage(R.string.ca3_step8);
				c.hideNote();
				c.showExtendedOptions(R.string.str_skip_this);
				c.setSteps(this, step8option);									
			}
		};
		
		final OnClickListener step8option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.getOption() == 1) {
					remove(R.string.Cr);					
				}			
				step9a.onClick(v);				
			}
		};
		
		final OnClickListener step9a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Al);
				c.setTitle(R.string.ca3_title9a);
				c.setMessage(R.string.ca3_step9a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step9aOption);													
			}
		};
		
		final OnClickListener step9aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Al);
					step10.onClick(v);
					break;
				case 1:
					step10.onClick(v);
					break;
				case 2:
					step9b.onClick(v);
				}
			}			
		};
		
		final OnClickListener step9b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Al);
				c.setTitle(R.string.ca3_title9b);
				c.setMessage(R.string.ca3_step9b);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step9bOption);													
																	
			}
		};
		
		final OnClickListener step9bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Al);
				}
				step10.onClick(v);				
			}
		};
		
		final OnClickListener step10 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title10);
				c.setMessage(R.string.ca3_step10);
				c.showNote(R.string.ca3_note10);
				c.showOptions();
				c.setSteps(this, step10option);													
			}
		};
		
		final OnClickListener step10option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step11.onClick(v);
				} else {
					ca4.step1.onClick(v);
				}
				
			}
		};
		
		final OnClickListener step11 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title11);
				c.setMessage(R.string.ca3_step11);
				c.showNote(R.string.ca3_note11);
				c.showOptions();
				c.setSteps(this, step11option);																	
			}
		};
		
		final OnClickListener step11option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step12.onClick(v);
				} else {
					step15.onClick(v);
				}
			}
		};
		
		final OnClickListener step12 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title12);
				c.setMessage(R.string.ca3_step12);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step13);
			}
		};
		
		final OnClickListener step13 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Co);
				c.setTitle(R.string.ca3_title13);
				c.setMessage(R.string.ca3_step13);
				c.showNote(R.string.ca3_note13);
				c.showOptions();
				c.setSteps(this, step13option);																	
			}
		};
		
		final OnClickListener step13option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Co);
				}
				step14.onClick(v);				
			}
		};
				
		final OnClickListener step14 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Ni);
				c.setTitle(R.string.ca3_title14);
				c.setMessage(R.string.ca3_step14);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step14option);				
			}
		};
		
		final OnClickListener step14option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ni);
				}
				step15.onClick(v);
			}
		};
		
		final OnClickListener step15 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title15);
				c.setMessage(R.string.ca3_step15);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step15option);					
			}
		};
		
		final OnClickListener step15option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step16.onClick(v);
				} else {
					step17.onClick(v);
				}
			}
		};
		
		final OnClickListener step16 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title16);
				c.setMessage(R.string.ca3_step16);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step16a);									
			}
		};
		
		final OnClickListener step16a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title6a);
				c.setMessage(R.string.ca3_step6a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step16aOption);													
			}
		};
		
		final OnClickListener step16aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Mn);
					step17.onClick(v);
					break;
				case 1:
					step17.onClick(v);
					break;
				case 2:
					step16b.onClick(v);
				}
			}
		};
		
		final OnClickListener step16b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Mn);
				c.setTitle(R.string.ca3_title6b);
				c.setMessage(R.string.ca3_step6b);
				c.showNote(R.string.ca3_note6b);
				c.showOptions();
				c.setSteps(this, step16bOption);																	
			}
		};
		
		final OnClickListener step16bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Mn);
				}
				step17.onClick(v);
			}
		};
 		
		final OnClickListener step17 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca3_title17);
				c.setMessage(R.string.ca3_step17);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step18a);																					
			}
		};
		
		final OnClickListener step18a = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				remove(R.string.Zn);
				c.setTitle(R.string.ca3_title18a);
				c.setMessage(R.string.ca3_step18a);
				c.hideNote();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step18aOption);																									
			}
		};
		
		final OnClickListener step18aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Zn);
					ca4.step1.onClick(v);
					break;
				case 1:
					ca4.step1.onClick(v);
					break;
				case 2:
					step18b.onClick(v);
				}
			}
		};
		
		final OnClickListener step18b = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				remove(R.string.Zn);
				c.setTitle(R.string.ca3_title18b);
				c.setMessage(R.string.ca3_step18b);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step18bOption);																									
			}
		};
		
		final OnClickListener step18bOption = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Zn);
				}
				ca4.step1.onClick(v);
			}
		};				
	} // end of Group 3
	
	
	class Group4 {
		
		final OnClickListener step1 = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title1);
				c.setMessage(R.string.ca4_step1);
				c.showNote(R.string.ca4_note1);
				c.hideOptions();
				if (alloy == null)
					c.setSteps(this, step2);
				else
					c.setSteps(this, ca5.step2);
			}
		};
		
		final OnClickListener step2 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title1);
				c.setMessage(R.string.ca4_step2);
				c.showNote(R.string.ca4_note2);
				c.showOptions();
				c.setSteps(this, step2option);																									
			}
		};
		
		final OnClickListener step2option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step3.onClick(v);
				} else {
					ca5.step1.onClick(v);
				}
			}
		};
		
		final OnClickListener step3 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title3);
				c.setMessage(R.string.ca4_step3);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step4);																									
				
			}
		};
		
		final OnClickListener step4 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Ba);
				c.setTitle(R.string.ca4_title4);
				c.setMessage(R.string.ca4_step4);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step4option);																													
			}
		};
		
		final OnClickListener step4option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ba);
					step5.onClick(v);
				} else {
					step6_1.onClick(v);
				}
			}
		};
		
		final OnClickListener step5 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title5);
				c.setMessage(R.string.ca4_step5);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step6_1);																																		
			}
		};
		
		final OnClickListener step6_1 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Sr);
				if (isProven(R.string.Ba)) {
					c.setTitle(R.string.ca4_title6b);
				} else {
					c.setTitle(R.string.ca4_title6a);
				}
				
				c.setMessage(R.string.ca4_step6_1);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step6option);																																		
			}
		};
		
		final OnClickListener step6option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Sr);
					step6_2.onClick(v);
				} else {
					step7b.onClick(v);
				}
			}
		}; 
		
		final OnClickListener step6_2 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title6_2);
				c.setMessage(R.string.ca4_step6_2);
				c.showNote(R.string.ca4_note6_2);
				c.hideOptions();
				c.setSteps(this, step7a);																																						
			}
		};
		
		final OnClickListener step7a = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Ca);
				c.setTitle(R.string.ca4_title7);
				c.setMessage(R.string.ca4_step7a);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step7aOption);				
			}
		}; 
		
		final OnClickListener step7aOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ca);
					step8.onClick(v);
				} else {
					ca5.step1.onClick(v);
				}				
			}
		};
		
		final OnClickListener step8 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title7);
				c.setMessage(R.string.ca4_step8);
				c.showNote(R.string.ca4_note8);
				c.showOptions();
				c.setSteps(this, step8option);
			}
		};
		
		final OnClickListener step8option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!c.isYes()) {
					remove(R.string.Ca);
				} 
				
				ca5.step1.onClick(v);
			}
		};
		
		final OnClickListener step7b = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca4_title7);
				c.setMessage(R.string.ca4_step7b);
				c.hideNote();
				c.showOptions();
				c.setSteps(this, step7bOption);
			}
		};
		
		final OnClickListener step7bOption = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Ca);
				}
				ca5.step1.onClick(v);
			}
		};
	
	} // end of Group 4
	
	
	class Group5 {
		
		final OnClickListener step1 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca5_title1);
				c.setMessage(R.string.ca5_step1);
				c.hideNote();
				c.hideOptions();
				c.setSteps(this, step2);							
			}
		};
		
		final OnClickListener step2 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Mg);
				c.setTitle(R.string.ca5_title2);
				c.setMessage(R.string.ca5_step2);
				c.showNote(R.string.ca5_note2);
				c.showOptions();
				c.setSteps(this, step2option);
			}
		};
		
		final OnClickListener step2option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Mg);
					step5.onClick(v);
				} else {
					step3.onClick(v);
				}				
			}
		};
		
		final OnClickListener step3 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca5_title2);
				c.setMessage(R.string.ca5_step3);
				c.hideNote();				
				c.showOptions();
				c.setSteps(this, step3option);						
			}
		};
		
		final OnClickListener step3option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					step4.onClick(v);
				} else {
					step5.onClick(v);
				}
			}
		};
		
		final OnClickListener step4 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				remove(R.string.Mg);
				c.setTitle(R.string.ca5_title2);
				c.setMessage(R.string.ca5_step4);
				c.showNote(R.string.ca5_note4);				
				c.showOptions();
				c.setSteps(this, step4option);														
			}
		};
		
		final OnClickListener step4option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (c.isYes()) {
					add(R.string.Mg);					
				}
				
				if (alloy == null)
					step5.onClick(v);
				else
					alloy.outro.onClick(v);					
			}
		};
		
		final OnClickListener step5 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca5_title5);
				c.setMessage(R.string.ca5_step5);
				c.hideNote();				
				c.hideOptions();
				c.setSteps(this, step6);																						
			}
		};
		
		final OnClickListener step6 = new OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(R.string.Na);
				remove(R.string.Li);
				remove(R.string.K);
				c.setTitle(R.string.ca5_title6);
				c.setMessage(R.string.ca5_step6);
				c.showNote(R.string.ca5_note6);				
				c.setOptionText(R.id.rdbYes, R.string.ca5_step6_option1);
				c.setOptionText(R.id.rdbNo, R.string.ca5_step6_option2);
				c.setOptionText(R.id.rdbOption1, R.string.ca5_step6_option3);
				c.showExtendedOptions();
				c.setSteps(this, step6option);																						
			}
		};
		
		final OnClickListener step6option = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				switch (c.getOption()) {
				case 0:
					add(R.string.Na);
					break;
				case 1:
					add(R.string.Li);
					break;
				case 2:
					add(R.string.K);
					break;
				}
				step7.onClick(v);				
			}
		};
		
		final OnClickListener step7 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca5_title7);
				if (isProven(R.string.NH4))
					c.setMessage(R.string.ca5_step7a);
				else
					c.setMessage(R.string.ca5_step7b);
				c.hideNote();				
				c.resetOptionText();
				c.showExtendedOptions(R.string.str_try_another);
				c.setSteps(this, step7option);																								
			}
		};
		
		final OnClickListener step7option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.getOption() == 0) {
					add(R.string.K);
					outro.onClick(v);
				} else {
					step8.onClick(v);
				}
			}
		};
		
		final OnClickListener step8 = new OnClickListener() {			
			@Override
			public void onClick(View v) {
				c.setTitle(R.string.ca5_title7);
				if (isProven(R.string.NH4))
					c.setMessage(R.string.ca5_step8a);
				else
					c.setMessage(R.string.ca5_step8b);
				c.hideNote();
				c.resetOptionText();
				c.showExtendedOptions(R.string.str_skip_this);
				c.setSteps(this, step8option);																																
			}
		};

		final OnClickListener step8option = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (c.getOption() == 0) {
					add(R.string.K);
				}
				outro.onClick(v);
			}
		};
		
		final OnClickListener outro = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.setSteps(this, endCations);
				c.setMessage(R.string.msg_outro_cations);
				c.hideTitle();		
				c.hideNote();
				c.hideOptions();
			}
		};
		
		final OnClickListener endCations = new OnClickListener() {
			@Override
			public void onClick(View v) {
				c.exit();
			}
		};
		
	}
	
}


