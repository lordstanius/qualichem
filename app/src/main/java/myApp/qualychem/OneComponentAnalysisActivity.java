package myApp.qualychem;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class OneComponentAnalysisActivity extends Activity {
	Controller controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.analysis_layout);
		
		controller = new Controller(this);
		
		new OneComponentAnalysis(controller).start();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.one_component_analysis, menu);
		return true;
	}

}
