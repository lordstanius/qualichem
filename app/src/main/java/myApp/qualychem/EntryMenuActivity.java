package myApp.qualychem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.widget.Toast;

public class EntryMenuActivity extends Activity {
	Intent cationAnalysis;
	Intent anionAnalysis;
	Intent alloyAnalysis;
	Intent oneComponentAnalysis;
	//Intent aboutDialog;
	
	OnClickListener onCations = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(cationAnalysis);		
		}
	};
	
	OnClickListener onAnions = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(anionAnalysis);
		}
	};
	
	OnClickListener onAlloy = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(alloyAnalysis);
		}
	};
	
	OnClickListener onOneComponent = new OnClickListener() {
		@Override
		public void onClick(View v) {
			startActivity(oneComponentAnalysis);
		}
	};

	OnClickListener onAbout =  new OnClickListener() {
		@Override
		public void onClick(View v) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
			builder.setPositiveButton(R.string.msg_about_btn, null);
			builder.setMessage(R.string.msg_about)
					.setTitle(R.string.str_about);
			try {
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			catch (Exception ex) {
				Toast.makeText(getApplicationContext(), R.string.msg_about, Toast.LENGTH_LONG).show();
			}

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.entry_menu_layout);
		
		findViewById(R.id.btn_cations).setOnClickListener(onCations);
		findViewById(R.id.btn_anions).setOnClickListener(onAnions);
		findViewById(R.id.btn_alloy).setOnClickListener(onAlloy);
		findViewById(R.id.btn_one_component).setOnClickListener(onOneComponent);
		findViewById(R.id.btn_about).setOnClickListener(onAbout);
		
		cationAnalysis       = new Intent(this, CationAnalysisActivity.class);
		anionAnalysis        = new Intent(this, AnionAnalysisActivity.class);
		alloyAnalysis        = new Intent(this, AlloyAnalysisActivity.class);
		oneComponentAnalysis = new Intent(this, OneComponentAnalysisActivity.class);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.entry_menu, menu);
		return true;
	}
}
