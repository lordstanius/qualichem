package myApp.qualychem;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class AnionAnalysisActivity extends Activity {
	Controller controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.analysis_layout);
		
		controller = new Controller(this);
		
		new AnionAnalysis(controller).start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
//	public void sendMessage(View view) {
//		Intent intent = new Intent(this, DisplayMessageActivity.class);
//		String message = ((EditText) findViewById(R.id.edit_message)).getText().toString();
//		intent.putExtra(EXTRA_MESSAGE, message);
//		startActivity(intent);	
//	}

}
