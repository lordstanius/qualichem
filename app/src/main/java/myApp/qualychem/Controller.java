package myApp.qualychem;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class Controller {

	//---------------------------------
	//  Private fields
	//---------------------------------
	private TextView    txtMessage;
	private TextView    txtTitleBar;
	private TextView    txtNote;
	private TextView    txtCations;
	private TextView    txtAnions;
	private RadioGroup  radioGroup;
	private Button      btnNext;
	private Button      btnBack;
	private Activity    source;

	private Stack<OnClickListener> stepsTaken = new Stack<OnClickListener>();
	
	private boolean isBackOpen = false;
	//private LinkedList<OnClickListener> stepsTaken;
	
	private OnClickListener last = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (!stepsTaken.isEmpty()) {
				if (!isBackOpen) {
					stepsTaken.pop();
					isBackOpen = true;
				}
				stepsTaken.pop().onClick(v);	
			}
		}
	};

	
	//---------------------------------
	//  Constructor
	//---------------------------------
	public Controller(Activity source) {
		this.source = source;
		
		txtMessage = (TextView)source.findViewById(R.id.txtMessage);
		txtTitleBar = (TextView)source.findViewById(R.id.txtTitleBar);
		txtNote = (TextView)source.findViewById(R.id.txtNote);
		txtCations = (TextView)source.findViewById(R.id.txtProvenCations);
		txtAnions = (TextView)source.findViewById(R.id.txtProvenAnions);
		radioGroup = (RadioGroup)source.findViewById(R.id.radioGroup);
		btnNext = (Button)source.findViewById(R.id.btnNext);
		btnBack = (Button)source.findViewById(R.id.btnBack);		
		btnBack.setOnClickListener(last);
	}
	
	public String getText(int stringId) {
		return source.getString(stringId);
	}
	
	public void setMessageTextSize(float sp) {
		txtMessage.setTextSize(sp);
	}

	/**
	 * Sets text of the TextView with HTML formated string. 
	 * @param stringId String resource ID (e.g. R.string.some_string)
	 */
	public void setMessage(int stringId) {
		txtMessage.setText(Html.fromHtml(getText(stringId)), BufferType.SPANNABLE);
		txtMessage.setClickable(false);
	}

	public void setMessage(int stringId, int drawableId) {
		txtMessage.setText(Html.fromHtml(getText(stringId)), BufferType.SPANNABLE);
		setPicture(txtMessage, drawableId);
	}
	
	public void setTitle(int stringId) {
		txtTitleBar.setText(Html.fromHtml(getText(stringId)), BufferType.SPANNABLE);
		txtTitleBar.setVisibility(View.VISIBLE);		
	}
	
	public void setTitle(String title) {
		txtTitleBar.setText(Html.fromHtml(title), BufferType.SPANNABLE);
		txtTitleBar.setVisibility(View.VISIBLE);
	}
	
	public void showTitle() {
		txtTitleBar.setVisibility(View.VISIBLE);
	}
	
	public void hideTitle() {
		txtTitleBar.setVisibility(View.GONE);
	}

	public void hideOptions() {
		radioGroup.setVisibility(View.GONE);
	}
	
	public void showOptions() {
		radioGroup.setVisibility(View.VISIBLE);
		source.findViewById(R.id.rdbOption1).setVisibility(View.GONE);
	}
	
	public void showExtendedOptions(int stringId) {
		radioGroup.setVisibility(View.VISIBLE);
		source.findViewById(R.id.rdbOption1).setVisibility(View.VISIBLE);
		
		((RadioButton) source.findViewById(R.id.rdbOption1)).setText(stringId);
	}
	
	public void showExtendedOptions() {
		radioGroup.setVisibility(View.VISIBLE);
		source.findViewById(R.id.rdbOption1).setVisibility(View.VISIBLE);
	}
	
	public void showNote(int stringId) {
		txtNote.setText(Html.fromHtml(source.getString(stringId)));
		txtNote.setVisibility(View.VISIBLE);
	}
	
	public void hideNote() {
		txtNote.setVisibility(View.GONE);
	}
	
	public void showCations() {
		txtCations.setVisibility(View.VISIBLE);		
	}
	
	public void hideCations() {
		txtCations.setVisibility(View.GONE);		
	}

	public void showAnions() {
		txtAnions.setVisibility(View.VISIBLE);		
	}

	public void hideAnions() {
		txtAnions.setVisibility(View.GONE);		
	}

	public void writeCations(Analysis provenCations) {
		txtCations.setText(Html.fromHtml(source.getString(R.string.proven_cations) + provenCations.toString()));
	}

	public void writeAnions(Analysis provenAnions) {
		txtAnions.setText(Html.fromHtml(source.getString(R.string.proven_anions) + provenAnions.toString()));
	}

	/**
	 * Retrieves the state of dual "Yes/No" radio button group selection.
	 * @return <code>true</code> if option "Yes" is checked, <code>false</code> otherwise.
	 */
	public boolean isYes() {
		return ((RadioButton) source.findViewById(R.id.rdbYes)).isChecked();
	}
	
	/** Returns selected radio button index */
	public int getOption() {
		int i = 0;
		for (int c = radioGroup.getChildCount(); i<c; ++i) {
			if (((RadioButton) radioGroup.getChildAt(i)).isChecked())
				break;
		}
		
		return i;
	}
	
	public void setOptionText(int optionId, String text) {
		((RadioButton) source.findViewById(optionId)).setText(text);
	}

	public void setOptionText(int optionId, int stringId) {
		((RadioButton) source.findViewById(optionId)).setText(stringId);
	}
	
	/**
	 * Changes text to it's default value for all options
	 */
	public void resetOptionText() {
		((RadioButton) source.findViewById(R.id.rdbYes)).setText(R.string.str_yes);
		((RadioButton) source.findViewById(R.id.rdbNo)).setText(R.string.str_no);
	}
	
	public void disableNext() {
		btnNext.setEnabled(false);
	}
	
	public void enableNext() {
		btnNext.setEnabled(true);
	}

	public void disableBack() {
		btnBack.setEnabled(false);	
	}
	
	public void enableBack() {
		btnBack.setEnabled(true);
	}
	
	/**
	 * Attaches appropriate <code>OnClickListener</code> to a buttons in the interface. Current <code>OnClickListener</code> for every
	 * step is pushed on the stack to make backtracking possible, and also OnClickListener for the next step is
	 * attached to "Next" button.
	 * @param current Always pass <code>this</code> or <code>null</code> for this parameter for backtracking to work as it should.
	 * @param next This param should be <code>OnClickListener</code> for "Next" button, i.e. next step. Can be <code>null</code>
	 */
	public void setSteps(OnClickListener current, OnClickListener next) {
		if (next != null) {
			btnNext.setEnabled(true);
			btnNext.setOnClickListener(next);
		} else {
			btnNext.setEnabled(false);
		}
		
		if (current != null) {
			stepsTaken.push(current);
			isBackOpen = false;
			if (stepsTaken.size() <= 1)
				btnBack.setEnabled(false);
			else
				btnBack.setEnabled(true);
		} else {
			btnBack.setEnabled(false);
		}
		
		// reset radio button group to it's default position
		((RadioButton) source.findViewById(R.id.rdbYes)).setChecked(true);
	}
	
	public void setNext(OnClickListener next) {
		if (next != null) {
			btnNext.setEnabled(true);
			btnNext.setOnClickListener(next);
		} else {
			btnNext.setEnabled(false);
		}		
	}
	
	/** Quits activity */
	public void exit() {
		source.finish();
	}
		
	
	//-------------------------------
	// Private methods
	//-------------------------------
	
	/** 
	 * Attach <code>OnClickListener</code> to a View. When clicked, a new Intent is created and based on that intent
	 * a new Activity started which displays a picture specified in the second parameter.
	 * @param v View to which the <code>OnClickListener</code> will be attached. 
	 * @param drawableId ID of the image resource that will be shown
	 */
	private void setPicture(View v, final int drawableId) {
		OnClickListener onClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent showPicture = new Intent(source, ShowPictureActivity.class);
				showPicture
					.putExtra("myApp.qualychem.ImageName", drawableId)
					.putExtra("myApp.qualychem.ActivityName", source.getTitle());
				
				source.startActivity(showPicture);
			}
		};
		
		v.setOnClickListener(onClickListener);
		//txtTitleBar.setBackgroundColor(Color.rgb(53, 0, 63));
	}
}





