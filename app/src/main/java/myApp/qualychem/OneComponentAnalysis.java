package myApp.qualychem;

import android.view.View;
import android.view.View.OnClickListener;

public class OneComponentAnalysis {
	Controller c;
	boolean   isColored = false;
	
	CationAnalysis    cations;
	AnionAnalysis2    anions;
	SampleDissolution sd;
		
	OneComponentAnalysis(Controller c) {
		this.c = c;
		sd = new SampleDissolution(c);
		cations = new CationAnalysis(c, this);
	}
	
	
	void start() {
		c.setSteps(null, step0);
		c.setMessage(R.string.msg_wellcome_one_component);
		c.hideOptions();
		c.hideNote();		
	}
	
	OnClickListener step0 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.oc_title1);
			c.setMessage(R.string.oc_step1);
			c.setOptionText(R.id.rdbYes, R.string.oc_step1_option1);
			c.setOptionText(R.id.rdbNo, R.string.oc_step1_option2);
			c.showOptions();
			c.showNote(R.string.oc_note1);
			c.setSteps(this, step0option);	
		}
	};	
	
	OnClickListener step0option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			c.resetOptionText();
			if (c.isYes()) {
				isColored = true;
			} else {
				isColored = false;
			}
			
			cations.ca1.step2.onClick(v); // proving of NH3	
		}
	};
	
	OnClickListener step1 = new OnClickListener() {  // dissolution of the sample 	
		@Override  
		public void onClick(View v) {
			sd.step1();
			c.setSteps(this, step1option);			
		}
	};
	
	OnClickListener step1option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) { 
				cations.ca1.step10.onClick(v); // everything has dissolved
			} else {
				step2.onClick(v);
			}			
		}
	};
	
	OnClickListener step2 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			sd.step2();
			c.setSteps(this, step2option);			
		}
	};	

	OnClickListener step2option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				cations.ca1.step10.onClick(v); // everything has dissolved											
			} else {
				step3.onClick(v);		
			}			
		}
	};
	
	OnClickListener step3 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			sd.step3();
			c.setSteps(this, step3option);			
		}
	};	
	
	OnClickListener step3option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				cations.ca1.step11.onClick(v); 
				// this one should ask if there is undissolved residue
				// if there is, than stop and join the rest of the solution
				// to residue dissolved in HNO3 or aqua regia.
			} else {
				step4.onClick(v);
			}			
		}
	};
	
	OnClickListener step4 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			sd.step4();
			c.setSteps(this, step4option);
		}
	};
	
	OnClickListener step4option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step9a.onClick(v);
			} else {
				step5.onClick(v);
			}
		}
	};
	
	OnClickListener step5 = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			sd.step5();
			c.setSteps(this, step5option);
		}
	};
	
	OnClickListener step5option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step9a.onClick(v);
			} else {
				step6.onClick(v);
			}
		}
	};
	
	OnClickListener step6 = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			sd.step6();
			c.setSteps(this, step6option);
		}
	};
	
	OnClickListener step6option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step9.onClick(v);
			} else {
				step7.onClick(v);
			}
		}
	};
	
	OnClickListener step7 = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			sd.step7();
			c.setSteps(this, step7option);
		}
	};
	
	OnClickListener step7option = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step9a.onClick(v);
			} else {
				step8.onClick(v);
			}
		}
	};
	
	OnClickListener step8 = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			sd.step8();
			c.setSteps(this, step9a);
		}
	};
	
	OnClickListener step9 = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			sd.step9();
			c.setSteps(this, cations.ca2.step2);
		}
	};

	OnClickListener step9a = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			sd.step9a();
			c.setSteps(this, cations.ca2.step2);
		}
	};

	OnClickListener stepNN = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			// TODO This is where begins analysis of 
			// cations II after sample dissolution
			
		}
	};
	
	OnClickListener stepAA = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			// TODO This is where anion analysis begins
			
		}
	};
	
	
}
