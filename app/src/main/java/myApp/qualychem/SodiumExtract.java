package myApp.qualychem;

import android.view.View;
import android.view.View.OnClickListener;


public class SodiumExtract {
	Controller c;
	
	
	SodiumExtract(Controller c) {
		this.c = c;
	}
	
	OnClickListener step1 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step1);
			c.hideOptions();
			c.hideNote();
		}
	};
	
	OnClickListener step1a = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step1a);
			c.hideOptions();
			c.hideNote();			
		}
	};
	
	OnClickListener step2 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step2);
			c.hideOptions();
			c.hideNote();			
		}
	};
	
	OnClickListener step2a = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step2a);
			c.hideOptions();
			c.hideNote();						
		}		
	};

	OnClickListener step3 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step3);
			c.hideOptions();
			c.hideNote();						
		}		
	};


	OnClickListener step4 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title1);
			c.setMessage(R.string.se_step3);
			c.hideOptions();
			c.hideNote();			
		}		
	};
	
	OnClickListener step5 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title5);
			c.setMessage(R.string.se_step5);
			c.hideOptions();
			c.hideNote();						
		}		
	};
	
	OnClickListener step6 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.se_title5);
			c.setMessage(R.string.se_step6);
			c.hideOptions();
			c.hideNote();			
		}		
	};
	
}
