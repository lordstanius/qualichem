package myApp.qualychem;


public class SampleDissolution {
	Controller c;
	
	
	SampleDissolution(Controller c) {
		this.c = c;
	}
		
	void step1() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step1);
		c.hideNote();
		c.showOptions();
	}
	
	
	void step2() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step2);
		c.hideNote();
		c.showOptions();
	}
	
	void step3() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step3);
		c.hideNote();
		c.showOptions();
	}
	
	void step4() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step4);
		c.hideNote();
		c.showOptions();		
	}
	
	void step4yes() {
		c.setTitle(R.string.ca2_title2);
		c.setMessage(R.string.sd_step4yes);
		c.hideNote();
		c.hideOptions();
	}
		
	void step5() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step5);
		c.hideNote();
		c.showOptions();
	}
	
	void step6() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step6);
		c.hideNote();
		c.showOptions();		
	}
	
	void step7() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step7);
		c.hideNote();
		c.showOptions();		
	}
	
	void step8() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step8);
		c.hideNote();
		c.hideOptions();		
	}
	
	void step9() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step9);
		c.showNote(R.string.sd_note9);
		c.hideOptions();		
	}
	
	void step9a() {
		c.setTitle(R.string.sd_title1);
		c.setMessage(R.string.sd_step9a);
		c.showNote(R.string.sd_note9);
		c.hideOptions();		
	}
	
//	OnClickListener step1 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step1);
//			c.hideNote();
//			c.showOptions();
//
//		}
//	};
//	
//	OnClickListener step2 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step2);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//	
//	OnClickListener step3 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step3);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//
//	OnClickListener step4 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step4);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//	
//	OnClickListener step5 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step5);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//	
//	OnClickListener step6 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step6);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//	
//	OnClickListener step7 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step7);
//			c.hideNote();
//			c.showOptions();
//		}
//	};
//	
//	OnClickListener step8 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step8);
//			c.hideNote();
//			c.showOptions();
//		}	
//	};
//	
//	OnClickListener step9 = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step2);
//			c.showNote(R.string.sd_note9);
//			c.showOptions();
//		}
//	};	
//
//	OnClickListener step9a = new OnClickListener() {		
//		@Override
//		public void onClick(View v) {
//			c.setTitle(R.string.sd_title1);
//			c.setMessage(R.string.sd_step9a);
//			c.showNote(R.string.sd_note9a);
//			c.showOptions();
//		}
//	};	
}

