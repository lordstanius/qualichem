package myApp.qualychem;

import java.util.ArrayList;

public abstract class Analysis {
	
	Controller c;
	
	private ArrayList<String> proven = new ArrayList<String>();
	
	
	//-----------------
	// Constructor
	//-----------------
	public Analysis(Controller controller) {
		c = controller;
	}
	
	abstract void refreshContent();
	
	public String toString() {
		String out = "";
		for (int i = 0; i < proven.size(); ++i) {
			if (i > 0 && i < proven.size())
				out += ", ";
			out += proven.get(i);
		}
		return out;
	}

	public void add(int stringId) {
		String newEntry = c.getText(stringId);
		if (proven.contains(newEntry) == false) {
			proven.add(newEntry);		
			refreshContent();
		}
	}

	public void remove(int stringId) {
		proven.remove(c.getText(stringId));
		refreshContent();
	}
	
	public boolean isProven(int stringId) {
		return (proven.contains(c.getText(stringId)));
	}
	
	public boolean isEmpty() {
		return proven.isEmpty();
	}

}
