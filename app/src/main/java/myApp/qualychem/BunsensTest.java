package myApp.qualychem;

import java.util.ArrayList;

import android.view.View;
import android.view.View.OnClickListener;

public class BunsensTest {
	Controller c;
	Analysis  caller;
	OnClickListener next;
	ArrayList<Integer> ionsToTest;
		
	BunsensTest(Analysis caller, OnClickListener next) {
		this.c = caller.c;
		this.caller = caller;
		this.next = next;
	}
	
	/** 
	 * Set <code>OnClickListener</code> that will be next step after all steps 
	 * in preparation of sodium extract are completed. 
	 */
	

	OnClickListener step1 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title1);
			c.setMessage(R.string.bz_step1);
			c.showOptions();
			c.hideNote();						
		}
	};
	
	OnClickListener option1 = new OnClickListener() {
		public void onClick(View v) {
			if (c.isYes()) {					
				ionsToTest.add(R.string.chloride);	
				ionsToTest.add(R.string.bromide);
				ionsToTest.add(R.string.iodide);
				ionsToTest.add(R.string.sulfide);
				ionsToTest.add(R.string.carbonate);
				ionsToTest.add(R.string.oxalate);
				ionsToTest.add(R.string.tartarate);
				ionsToTest.add(R.string.sulfite);
				ionsToTest.add(R.string.sulfate);
				ionsToTest.add(R.string.thiosulfate);
				ionsToTest.add(R.string.acetate);
				ionsToTest.add(R.string.nitrite);
				ionsToTest.add(R.string.nitrate);
				ionsToTest.add(R.string.borate);
				ionsToTest.add(R.string.phosphate);
				ionsToTest.add(R.string.iodate);
				ionsToTest.add(R.string.bromate);
				ionsToTest.add(R.string.chlorate);
				step2.onClick(v);				
			} else {
				ionsToTest.add(R.string.carbonate);
				ionsToTest.add(R.string.acetate);
				ionsToTest.add(R.string.nitrate);
				ionsToTest.add(R.string.chlorate);
				ionsToTest.add(R.string.sulfate);	
				step5.onClick(v);
			}						
		}
	};
	
	
	OnClickListener step2 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title1);
			c.setMessage(R.string.bz_step2);
			c.hideOptions();
			c.hideNote();						
		}
	};
		
	OnClickListener step3 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title1);
			c.setMessage(R.string.bz_step3);
			c.showOptions();
			c.hideNote();			
		}
	};
		
	OnClickListener option3 = new OnClickListener() {
		public void onClick(View v) {
			if (c.isYes()) {
				ionsToTest.remove((Integer)R.string.chloride);
				ionsToTest.remove((Integer)R.string.bromide);
				ionsToTest.remove((Integer)R.string.iodide);
				ionsToTest.remove((Integer)R.string.sulfide);
				step5.onClick(v);
			} else {
				ionsToTest.remove(R.string.oxalate);
				ionsToTest.remove(R.string.tartarate);
				ionsToTest.remove(R.string.sulfite);
				ionsToTest.remove(R.string.thiosulfate);
				ionsToTest.remove(R.string.nitrite);
				ionsToTest.remove(R.string.borate);
				ionsToTest.remove(R.string.phosphate);
				ionsToTest.remove(R.string.iodate);
				ionsToTest.remove(R.string.bromate);				
			}
		}
	};
	
	OnClickListener step4 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title1);
			c.setMessage(R.string.bz_step4);
			c.showExtendedOptions();
			c.setOptionText(R.id.rdbYes, R.string.bz_step4_option1);
			c.setOptionText(R.id.rdbNo, R.string.bz_step4_option2);
			c.setOptionText(R.id.rdbOption1, R.string.bz_step4_option3);
			c.hideNote();			
			
		}
	};
	
	OnClickListener option4 = new OnClickListener() {
		public void onClick(View v) {
			switch (c.getOption()) {
			case 0:
				c.resetOptionText();
				step4a.onClick(v);
				break;
			case 1:
				c.resetOptionText();
				caller.add(R.string.iodide);
				next.onClick(v);
				break;
			case 2:
				c.resetOptionText();
				caller.add(R.string.sulfide);
				next.onClick(v);				
			}			
		}
	};

	OnClickListener step4a = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title1);
			c.setMessage(R.string.bz_step4a);
			c.showOptions();
			c.hideNote();			
		}
	};
	
	OnClickListener option4a = new OnClickListener() {
		public void onClick(View v) {
			if (c.isYes()) {
				caller.add(R.string.chloride);
				next.onClick(v);
			} else {
				caller.add(R.string.bromide);
				next.onClick(v);
			}			
		}
	};

	OnClickListener step5 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title5);
			c.setMessage(R.string.bz_step5);
			c.hideOptions();
			c.hideNote();			
		}
	};
	
	OnClickListener option5 = new OnClickListener() {
		public void onClick(View v) {
			if (c.isYes()) {
				step6.onClick(v);
			} else {
				//ionsToTest.remove(Integer(R.string.))
				next.onClick(v);
			}			
		}
	};
	

	OnClickListener step6 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.bz_title5);
			c.setMessage(R.string.bz_step6);
			c.hideOptions();
			c.hideNote();			
		}
	};
}
