package myApp.qualychem;

import java.util.ArrayList;

import android.view.View;
import android.view.View.OnClickListener;

public class AnionAnalysis2 extends Analysis {
	/** Halogenati su redukovani do halogenida */
	boolean isReduced = false;	
	
	boolean hasBromideOrNitrite = false;
	boolean hasAmmonia = false;
	
	SodiumExtract pse;
	ArrayList<Integer> hits;
	
	// ---------------------------------------
	// Constructor
	// ---------------------------------------
	public AnionAnalysis2(Controller controller) {
		super(controller);
		hits = new ArrayList<Integer>();
		hits.add(R.string.chloride);	
		hits.add(R.string.bromide);
		hits.add(R.string.iodide);
		hits.add(R.string.sulfide);
		hits.add(R.string.carbonate);
		hits.add(R.string.oxalate);
		hits.add(R.string.tartarate);
		hits.add(R.string.sulfite);
		hits.add(R.string.thiosulfate);
		hits.add(R.string.acetate);
		hits.add(R.string.nitrite);
		hits.add(R.string.nitrate);
		hits.add(R.string.borate);
		hits.add(R.string.phosphate);
		hits.add(R.string.iodate);
		hits.add(R.string.bromate);
		hits.add(R.string.chlorate);
		hits.add(R.string.sulfate);		
	}
			
	public void refreshContent() {
		if (!isEmpty()) {
			c.writeAnions(this);
			c.showAnions();
		} else {
			c.hideAnions();
		}
	}	
	
	boolean thereIsSomeReducingAnion() {
		return isProven(R.string.sulfide) || isProven(R.string.sulfite) || 
				isProven(R.string.thiosulfate) || isProven(R.string.oxalate) ||
				isProven(R.string.tartarate) || isProven(R.string.chloride) ||
				isProven(R.string.bromide) || isProven(R.string.iodide) ||
				isProven(R.string.nitrite);
	}
	
	
	final OnClickListener step2andHalf = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.an_title2andHalf);
			c.setMessage(R.string.an_step2andHalf);
			c.showExtendedOptions();
			c.setOptionText(R.id.rdbYes, R.string.an_step2andHalf_option1);
			c.setOptionText(R.id.rdbNo, R.string.an_step2andHalf_option2);
			c.setOptionText(R.id.rdbOption1, R.string.an_step2andHalf_option3);
			c.showNote(R.string.an_note2andHalf);			
			c.setSteps(this, step2andHalfOption);			
		}
	};
	
	final OnClickListener step2andHalfOption = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(c.getOption()) {	
			case 0:
				step3.onClick(v);
				break;
			case 1:
				step5a.onClick(v);
				break;
			case 2:
				step10.onClick(v);
			}									
		}
	};
	
	final OnClickListener step3 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.resetOptionText();
			c.setTitle(R.string.an_title3);
			c.setMessage(R.string.an_step3);
			c.hideOptions();
			c.hideNote();			
			c.setSteps(this, step4);			
		}
	};
		
	final OnClickListener step4 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.carbonate);					
			c.setTitle(R.string.an_title3);
			c.setMessage(R.string.an_step4);
			c.showOptions();
			c.showNote(R.string.an_note4);			
			c.setSteps(this, step4option);			
		}
	};
	
	final OnClickListener step4option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.carbonate);
			}
			step5.onClick(v);
		}
	};	
	
	final OnClickListener step5 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.sulfide);					
			c.setTitle(R.string.an_title5);
			c.setMessage(R.string.an_step5);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step5option);			
		}
	};
	
	final OnClickListener step5a = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.resetOptionText();
			remove(R.string.sulfide);					
			c.setTitle(R.string.an_title5);
			c.setMessage(R.string.an_step5a);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step5option);			
		}
	};

	final OnClickListener step5option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.sulfide);
			} 
			step10.onClick(v);
		}
	};	

	final OnClickListener step6 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.thiosulfate);					
			c.setTitle(R.string.an_title6);
			c.setMessage(R.string.an_step6);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step6option);			
		}
	};
	
	final OnClickListener step6option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.thiosulfate);
			}
			step8.onClick(v);
		}
	};		
	
	final OnClickListener step7 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.sulfate);					
			c.setTitle(R.string.an_title7);
			c.setMessage(R.string.an_step7);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step7option);			
		}
	};
	
	final OnClickListener step7a = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.sulfate);					
			c.setTitle(R.string.an_title7);
			c.setMessage(R.string.an_step7a);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step7aOption);			
		}
	};
	
	final OnClickListener step7option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.sulfate);
			}
			if (isProven(R.string.sulfide))
				step9.onClick(v);
			else
				step8.onClick(v);
		}
	};	
	
	final OnClickListener step7aOption = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.sulfate);
			}
			step9.onClick(v);
		}
	};	

	final OnClickListener step8 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.sulfite);					
			c.setTitle(R.string.an_title8);
			c.setMessage(R.string.an_step8);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step8option);			
		}
	};
	
	final OnClickListener step8option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.sulfite);
			}
			step7.onClick(v);
		}
	};	
	
	final OnClickListener step9 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			remove(R.string.phosphate);					
			c.setTitle(R.string.an_title9);
			c.setMessage(R.string.an_step9);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step9option);			
		}
	};
	
	final OnClickListener step9option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.phosphate);
			}
			if (isProven(R.string.tartarate))
				step15.onClick(v);
			else
				step16a.onClick(v);
		}
	};	
	
	final OnClickListener step10 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.an_title10);
			c.setMessage(R.string.an_step10);
			c.hideOptions();
			c.hideNote();			
			c.setSteps(this, step11);			
		}
	};
		
	final OnClickListener step11 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setTitle(R.string.an_title10);
			c.setMessage(R.string.an_step11a);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step12);			
		}
	};
	
	final OnClickListener step12 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.iodide);
			c.setTitle(R.string.an_title10);
			c.setMessage(R.string.an_step12);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step12option);			
		}
	};
	
	final OnClickListener step12option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.iodide);
			}
			step13.onClick(v);
		}
	};
	
	final OnClickListener step13 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title10);
			c.setMessage(R.string.an_step13);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step13option);			
		}
	};
	
	final OnClickListener step13option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				hasBromideOrNitrite = true;
			}
			step14.onClick(v);
		}
	};
		
	final OnClickListener step14 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.tartarate);
			c.setTitle(R.string.an_title14);
			c.setMessage(R.string.an_step14);
			c.showOptions();
			c.hideNote();			
			c.setSteps(this, step14option);			
		}
	};
	
	final OnClickListener step14option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.tartarate);
			}
			step19.onClick(v);
		}
	};
	
	final OnClickListener step15 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title15);
			c.setMessage(R.string.an_step15);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step15option);			
		}
	};
	
	final OnClickListener step15option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step16b.onClick(v);
			} else {
				step22.onClick(v);
			}
		}
	};
	
	final OnClickListener step16a = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title15);
			c.setMessage(R.string.an_step16a);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step17);			
		}
	};
	
	final OnClickListener step16b = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title15);
			c.setMessage(R.string.an_step16b);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step17);			
		}
	};
	
	final OnClickListener step17 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title15);
			c.setMessage(R.string.an_step17);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step18);
		}
	};
	
	final OnClickListener step18 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.oxalate);
			c.setTitle(R.string.an_title15);
			c.setMessage(R.string.an_step18);
			c.showOptions();
			c.showNote(R.string.an_note18);
			c.setSteps(this, step18option);			
		}
	};
	
	final OnClickListener step18option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.oxalate);
			}
			step22.onClick(v);
		}
	};
		
	final OnClickListener step19 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title19);
			c.setMessage(R.string.an_step19);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step20);			
		}
	};
	
	final OnClickListener step20 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.acetate);
			c.setTitle(R.string.an_title19);
			c.setMessage(R.string.an_step20);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step20option);			
		}
	};
	
	final OnClickListener step20option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.acetate);
			}
			step21.onClick(v);
		}
	};
	
	final OnClickListener step21 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.borate);
			c.setTitle(R.string.an_title21);
			c.setMessage(R.string.an_step21);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step21option);			
		}
	};
	
	final OnClickListener step21option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.borate);
			}
			
			pse = new SodiumExtract(c);
			if (isProven(R.string.sulfide)) // if sulfide is proven, skip proving sulfite and thiosulfata
				c.setNext(step7);
			else
				c.setNext(step6);

			
			if (hasAmmonia)
				pse.step1.onClick(v);
			else
				pse.step1a.onClick(v);
		}
	};
	
	final OnClickListener step22 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (!isReduced) {
				c.setTitle(R.string.an_title22a);
			} else {
				c.setTitle(R.string.an_title22b);
			}
			c.setMessage(R.string.an_step22);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step22option);			
		}
	};
	
	final OnClickListener step22a = new OnClickListener() {        // ovo je specijalni slucaj kada se izdvajaju mrke pare joda
		@Override
		public void onClick(View v) {					
			if (!isReduced) {
				c.setTitle(R.string.an_title23a);
			} else {
				c.setTitle(R.string.an_title23b);
			}
			c.setMessage(R.string.an_step22a);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step24);			
		}
	};
	
	final OnClickListener step22option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				step23.onClick(v);
			} else if (isReduced) {
				outro.onClick(v);
			} else {
				step28.onClick(v);
			}
		}
	};	
	
	final OnClickListener step23 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isReduced) {
				c.setTitle(R.string.an_title23b);
			} else {
				c.setTitle(R.string.an_title23a);
			}
			c.setMessage(R.string.an_step23);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step24);			
		}
	};
	
	final OnClickListener step24 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isReduced) {
				remove(R.string.chlorate);
				c.setTitle(R.string.an_title24b);
			} else {
				remove(R.string.chloride);
				c.setTitle(R.string.an_title24a);
			}
			
			c.showNote(R.string.an_note24);
			c.setMessage(R.string.an_step24a);
			c.showOptions();
			c.setSteps(this, step24option);
		}
	};
	
	final OnClickListener step24option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				if (isReduced) {
					add(R.string.chlorate);
					outro.onClick(v);
				} else {
					add(R.string.chloride);
					step28.onClick(v);
				}
			} else {
				step25.onClick(v);
			}			
		}
	};		

	final OnClickListener step25 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isReduced) {
				remove(R.string.chlorate);
				c.setTitle(R.string.an_title25b);
			} else {
				remove(R.string.chloride);
				c.setTitle(R.string.an_title25a);
			}			
			c.setMessage(R.string.an_step25);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step25option);			
		}
	};
	
	final OnClickListener step25option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				if (isReduced) {
					add(R.string.chlorate);
				} else {
					add(R.string.chloride);
				}
			} 
			step26.onClick(v);						
		}
	};		
	
	final OnClickListener step26 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isReduced) {
				remove(R.string.iodate);
				c.setTitle(R.string.an_title26b);
			} else {
				remove(R.string.iodide);
				c.setTitle(R.string.an_title26a);
			}			
			c.setMessage(R.string.an_step26);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step26option);			
		}
	};
	
	final OnClickListener step26option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				if (isReduced) {
					add(R.string.iodate);
				} else {
					add(R.string.iodide);
				}
			} 			
			step27.onClick(v);
			
		}
	};		

	final OnClickListener step27 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isReduced) {
				remove(R.string.bromate);
				c.setTitle(R.string.an_title27b);
			} else {
				remove(R.string.bromide);
				c.setTitle(R.string.an_title27a);
			}			
			c.setMessage(R.string.an_step27);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step27option);			
		}
	};
	
	final OnClickListener step27option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				if (isReduced) {
					add(R.string.bromate);
					outro.onClick(v);
				} else {
					add(R.string.bromide);
				}
			}
			if (isReduced) 
				outro.onClick(v);
			else
				step28.onClick(v);
		}
	};		

	final OnClickListener step28 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			if (isProven(R.string.bromide) || isProven(R.string.iodide)) {
				c.setTitle(R.string.an_title29);
				c.setMessage(R.string.an_step29);
			} else {
				c.setTitle(R.string.an_title28);
				c.setMessage(R.string.an_step28);
			}
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step30a);
			
		}
	};
	
	final OnClickListener step29 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title29);
			c.setMessage(R.string.an_step29);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step30a);			
		}
	};
	
	final OnClickListener step30a = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.nitrite);
			c.setTitle(R.string.an_title30a);
			c.setMessage(R.string.an_step30a);
			c.showExtendedOptions(R.string.str_try_another);
			c.hideNote();
			c.setSteps(this, step30aOption);			
		}
	};
	
	final OnClickListener step30aOption = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (c.getOption()) {
			case 0:
				add(R.string.nitrite);
				step31.onClick(v);
				break;
			case 1:
				step32.onClick(v);
				break;
			case 2:
				step30b.onClick(v);
			}
		}
	};

	final OnClickListener step30b = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.nitrite);
			c.setTitle(R.string.an_title30b);
			c.setMessage(R.string.an_step30b);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step30bOption);			
		}
	};
		
	final OnClickListener step30bOption = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.nitrite);
				step31.onClick(v);
			} else {
				step32.onClick(v);
			}
		}
	};

	final OnClickListener step31 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title31);
			c.setMessage(R.string.an_step31);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step32);			
		}
	};
	
	final OnClickListener step32 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			remove(R.string.nitrate);
			c.setTitle(R.string.an_title32);
			c.setMessage(R.string.an_step32);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step32option);			
		}
	};
	
	final OnClickListener step32option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) {
				add(R.string.nitrate);	
			}
			
			if (thereIsSomeReducingAnion()) // prisutni su redukujuci anjoni
				outro.onClick(v);
			else       //ako nisu, uradi test na halogenate
				step33.onClick(v);
		}
	};
	
	final OnClickListener step33 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			isReduced = false;
			c.setTitle(R.string.an_title33);
			c.setMessage(R.string.an_step33);
			c.showOptions();
			c.hideNote();
			c.setSteps(this, step33option);
		}
	};
	
	final OnClickListener step33option = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (c.isYes()) 
				step35.onClick(v);
			else
				outro.onClick(v);
		}
	};	
	
	final OnClickListener step34 = new OnClickListener() {
		@Override
		public void onClick(View v) {
			isReduced = false;
			c.setTitle(R.string.an_title34);
			c.setMessage(R.string.an_step34);
			c.hideOptions();
			c.hideNote();
			c.setSteps(this, step35);			
		}
	};
	
	final OnClickListener step35 = new OnClickListener() {
		@Override
		public void onClick(View v) {					
			c.setTitle(R.string.an_title35);
			c.setMessage(R.string.an_step35);
			c.hideOptions();
			c.hideNote();
			isReduced = true;
			c.setSteps(this, step23);			
		}
	};
		
	final OnClickListener outro = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.setSteps(this, endAnions);
			c.setMessage(R.string.msg_outro_anions);
			c.hideTitle();		
			c.hideNote();
			c.hideOptions();
		}
	};
	
	final OnClickListener endAnions = new OnClickListener() {
		@Override
		public void onClick(View v) {
			c.exit();
		}
	};	

}
