package myApp.qualychem;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class AlloyAnalysisActivity extends Activity {
	Controller controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.analysis_layout);
		
		controller = new Controller(this);
		
		new AlloyAnalysis(controller).start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alloy_analysis, menu);
		return true;
	}
}
